微信服务号SDK
====================

服务号消息回调

----------------------

目前只支持cowboy的web服务器，配置方法如下：

使用erl_weixin_protocol处理器

~~~~
#!erlang
Options = [
    {handler, my_weixin_handler},
    {token,  "Token"},
    {app_id, "AppId"},
    {aes_key, "Aes Key"}, % 如何需要支持加密的消息，则必须配置
    {secret, "App Secret"}
],

Dispatch = cowboy_router:compile([
    {'_', [
        {"/notepad", erl_weixin_protocol, Options}
    ]}
]),
{ok, _} = cowboy:start_http(http, 100, [{port, 80}], [
    {env, [{dispatch, Dispatch}]}
])
~~~~

消息处理模块my_weixin_handler.erl
把用户发送来的文本消息原样返回

~~~~
#!erlang
-module(my_weixin_handler).
-behaviour(erl_weixin_handler).
-export([weixin_init/0, weixin_message/3, weixin_event/3, weixin_terminate/2]).
-include_lib("erl_weixin/include/erl_weixin.hrl").

weixin_init() -> ok.

weixin_message(_MsgHeader, #weixin_text_message{content = Content}, State) ->
    {reply, #weixin_text_message{content = Content}, State};

weixin_message(_MsgHeader, _Msg, State) ->
    {noreply, State}.

weixin_event(_MsgHeader, _Event, State) ->
    {noreply, State}.

weixin_terminate(_Reason, _State) -> ok.

~~~~

服务号功能请求
------------------------

初始化微信功能请求模块

~~~~
#!erlang
{ok, Pid} = erl_weixin:start_link("AppId", "App Secret").
~~~~

**建议放入supervisor**

例子：获取用户列表

~~~~
#!erlang
{ok, UserList} = erl_weixin:get_user_list(Pid).
~~~~