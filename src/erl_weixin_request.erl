%% -*- coding: utf-8 -*-

-module(erl_weixin_request).
-export([json_rep/3, raw_rep/3, post/5, get/3]).
-include("erl_weixin_logger.hrl").

json_rep(Url, RepHeaders, JsonText) ->
	case rfc4627:decode(JsonText) of
		{ok, Json, _} ->
			case rfc4627:get_field(Json, "errcode", undefined) of
				undefined ->
					{ok, RepHeaders, Json};
				ErrorCode when ErrorCode == 0 ->
					{ok, RepHeaders, Json};
				ErrorCode ->
					ErrMsg = binary_to_list(rfc4627:get_field(Json, "errmsg", undefined)),
					?ERROR("Request ~s failed (~p): ~s", [Url, ErrorCode, ErrMsg]),
					{error, {weixin_error, ErrorCode, ErrMsg}}
			end;
		Error ->
			Error
	end.

raw_rep(_Url, RepHeaders, Data) ->
	{ok, RepHeaders, Data}.

post(Url, ContentType, Headers, PostData, BodyParse) ->
	case httpc:request(post, {Url, Headers, ContentType, PostData}, [], []) of
		{ok, {{_Version, 200, _ReasonPhrase}, RepHeaders, Body}} ->
			BodyParse(Url, RepHeaders, Body);
		Error ->
			Error
	end.

get(Url, Headers, BodyParse) ->
	case httpc:request(get, {Url, Headers}, [], []) of
		{ok, {{_Version, 200, _ReasonPhrase}, RepHeaders, Body}} ->
			BodyParse(Url, RepHeaders, Body);
		Error ->
			Error
	end.
