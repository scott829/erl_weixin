%% -*- coding: utf-8 -*-

-module(erl_weixin_signature).
-export([check/2, calc/1]).

check(Values, Signature) ->
	calc(Values) =:= Signature.

calc(Values) ->
	S = lists:flatten(lists:sort(Values)),
	Hash = binary_to_list(crypto:hash(sha, S)),
	lists:flatten(lists:map(fun(Byte) ->
		case io_lib:format("~.16b", [Byte]) of
			[[C]] -> [$0, C];
			R -> R
		end
	end, Hash)).