%% -*- coding: utf-8 -*-

-module(erl_weixin_http_multipart).
-export([boundary/0, create_from_file/3]).

boundary() -> binary_to_list(base64:encode(crypto:rand_bytes(48))).

create_from_file(Boundary, Filename, FileData) ->
	EndLine = [13, 10],
	ContentDisposition = io_lib:format("Content-Disposition: form-data; name=\"media\"; filename=\"~s\"", [Filename]),
	ContentType = "Content-Type: application/octet-stream",
	iolist_to_binary([
		"--", Boundary, EndLine,
		ContentDisposition, EndLine,
		ContentType, EndLine,
		EndLine,
		FileData,
		EndLine, "--", Boundary, "--", EndLine
	]).