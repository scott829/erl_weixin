%% -*- coding: utf-8 -*-

-module(erl_weixin_menu).
-export([to_json/1, from_json/1]).
-include("erl_weixin.hrl").

to_json(Buttons) ->
	lists:map(fun button_to_json/1, Buttons).

button_to_json(#weixin_button{type = click, name = Name, key = Key}) ->
	{obj, [
		{<<"type">>, <<"click">>},
		{<<"name">>, list_to_binary(Name)},
		{<<"key">>, list_to_binary(Key)}
	]};

button_to_json(#weixin_button{type = view, name = Name, url = Url}) ->
	{obj, [
		{<<"type">>, <<"view">>},
		{<<"name">>, list_to_binary(Name)},
		{<<"url">>, list_to_binary(Url)}
	]};

button_to_json(#weixin_button{type = undefined, name = Name, sub_buttons = SubButtons}) ->
	{obj, [
		{<<"name">>, list_to_binary(Name)},
		{<<"sub_button">>, to_json(SubButtons)}
	]};

button_to_json(#weixin_button{type = Type, name = Name}) ->
	{obj, [
		{<<"type">>, atom_to_binary(Type, utf8)},
		{<<"name">>, list_to_binary(Name)}
	]}.

from_json(Buttons) when is_list(Buttons) ->
	lists:map(fun(Button) ->
		Type = case rfc4627:get_field(Button, "type", undefined) of
				   undefined -> undefined;
				   TypeBinary -> binary_to_atom(TypeBinary, utf8)
			   end,
		Name = case rfc4627:get_field(Button, "name", undefined) of
				   undefined -> undefined;
				   NameBinary -> binary_to_list(NameBinary)
			   end,
		Key = case rfc4627:get_field(Button, "key", undefined) of
				  undefined -> undefined;
				  KeyBinary -> binary_to_list(KeyBinary)
			  end,
		Url = case rfc4627:get_field(Button, "url", undefined) of
				  undefined -> undefined;
				  UrlBinary -> binary_to_list(UrlBinary)
			  end,
		SubButtons = case rfc4627:get_field(Button, "sub_button", undefined) of
						 undefined -> undefined;
						 SubButtonsList -> from_json(SubButtonsList)
					 end,
		#weixin_button{
			type = Type,
			name = Name,
			key = Key,
			url = Url,
			sub_buttons = SubButtons
		}
	end, Buttons).