%% -*- coding: utf-8 -*-

-ifndef(erl_weixin_logger).
-define(erl_weixin_logger, true).

-define(INFO(Format, Args), error_logger:info_msg("~p: " ++ Format ++ "~n", [?MODULE | Args])).

-define(WARN(Format, Args), error_logger:warning_msg("~p: " ++ Format ++ "~n", [?MODULE | Args])).

-define(ERROR(Format, Args), error_logger:error_msg("~p: " ++ Format ++ "~n", [?MODULE | Args])).

-endif.
