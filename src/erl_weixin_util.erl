%% -*- coding: utf-8 -*-

-module(erl_weixin_util).
-export([time_zone/0, datetime_to_ctime/1, ctime_to_datetime/1, integer_to_boolean/1, sex/1, language/1]).

time_zone() -> 8 * 60 * 60.

ctime_base() ->
	calendar:datetime_to_gregorian_seconds({{1970, 1, 1}, {0, 0, 0}}).

datetime_to_ctime(DateTime) ->
	calendar:datetime_to_gregorian_seconds(DateTime) - ctime_base().

ctime_to_datetime(CTime) ->
	calendar:gregorian_seconds_to_datetime(ctime_base() + CTime).

integer_to_boolean(1) -> true;
integer_to_boolean(_) -> false.

sex(1) -> gentleman;
sex(2) -> lady;
sex(_) -> unknown.

language("zh_CN") -> zh_CN;
language("zh_TW") -> zh_TW;
language("en") -> en;
language(_) -> zh_CN.