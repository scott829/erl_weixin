%% -*- coding: utf-8 -*-

-module(erl_weixin).
-behaviour(gen_server).
-export([start_link/1, start_link/2]).
-export([init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3]).
-export([upload_media/4, download_media/2]).
-export([create_group/2, get_groups/1, get_user_group/2, rename_group/3, set_user_group/3, set_users_group/3]).
-export([set_user_remark/3, get_user_info/3, get_user_info/2, get_user_list/1, get_user_list/2]).
-export([create_menu/2, get_menu/1, delete_menu/1]).
-export([create_qrcode/4, get_qrcode_image/1, create_short_url/2]).
-include_lib("stdlib/include/qlc.hrl").
-include("erl_weixin_logger.hrl").
-include("erl_weixin.hrl").

-record(state, {
	app_id :: string(),
	secret :: string(),
	access_token :: string()
}).

-record(weixin_token, {
	app_id :: string(),
	access_token :: string(),
	expire_time :: calendar:datetime()
}).

create_tables() ->
	mnesia:create_table(weixin_token, [
		{type, set},
		{attributes, record_info(fields, weixin_token)},
		{disc_copies, [node()]}
	]).

start_link(Options) ->
	gen_server:start_link(?MODULE, [Options], []).

start_link(Name, Options) ->
	gen_server:start_link(Name, ?MODULE, [Options], []).

init([Options]) ->
	create_tables(),
	AppId = proplists:get_value(app_id, Options),
	Secret = proplists:get_value(secret, Options),
	case load_access_token(AppId, Secret) of
		{ok, {AccessToken, ExpireTime}} ->
			set_expire_timer(ExpireTime),
			{ok, #state{
				app_id = AppId,
				secret = Secret,
				access_token = AccessToken
			}};
		{error, Reason} ->
			{stop, Reason}
	end.

handle_call({upload_media, Filename, FileData, Type}, _From, State) ->
	UploadUrl = "http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=~s&type=~s",
	Url = lists:flatten(io_lib:format(UploadUrl, [State#state.access_token, Type])),
	Boundary = erl_weixin_http_multipart:boundary(),
	ContentType = lists:flatten(io_lib:format("multipart/form-data; boundary=~s", [Boundary])),
	PostData = erl_weixin_http_multipart:create_from_file(Boundary, Filename, FileData),
	?INFO("Upload ~s file ~s...", [Type, Filename]),
	case erl_weixin_request:post(Url, ContentType, [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			?INFO("Upload file ~s succeeded", [Filename]),
			MediaId = binary_to_list(rfc4627:get_field(RepJson, "media_id", undefined)),
			CreateAt = rfc4627:get_field(RepJson, "created_at", undefined),
			{reply, {ok, MediaId, erl_weixin_util:ctime_to_datetime(CreateAt + erl_weixin_util:time_zone())}, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({download_media, MediaId}, _From, State) ->
	DownloadUrl = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=~s&media_id=~s",
	Url = lists:flatten(io_lib:format(DownloadUrl, [State#state.access_token, MediaId])),
	?INFO("Download file ~s...", [MediaId]),
	case erl_weixin_request:get(Url, [], fun erl_weixin_request:raw_rep/3) of
		{ok, RepHeaders, Body} ->
			?INFO("Download file ~s succeeded", [MediaId]),
			ContentDisposition = proplists:get_value("content-disposition", RepHeaders),
			FilenamePos = string:str(ContentDisposition, "filename=\""),
			Filename = lists:takewhile(fun(Char) ->
				Char =/= $" end, string:sub_string(ContentDisposition, FilenamePos + 10)),
			ContentType = proplists:get_value("content-type", RepHeaders),
			{reply, {ok, #weixin_download_media{mime = ContentType, filename = Filename, data = Body}}, State};
		Error ->
			?ERROR("Failed to download media file: ~s", [MediaId]),
			{reply, Error, State}
	end;

handle_call({create_group, Name}, _From, State) ->
	CreateGroupUrl = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=~s",
	Url = lists:flatten(io_lib:format(CreateGroupUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"group">>, {obj, [
			{<<"name">>, list_to_binary(Name)}
		]}}
	]}),
	?INFO("Create users group ~s", [Name]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			Group = rfc4627:get_field(RepJson, "group", undefined),
			GroupId = rfc4627:get_field(Group, "id", undefined),
			{reply, {ok, GroupId}, State};
		Error ->
			{reply, Error, State}
	end;

handle_call(get_groups, _From, State) ->
	GetGroupsUrl = "https://api.weixin.qq.com/cgi-bin/groups/get?access_token=~s",
	Url = lists:flatten(io_lib:format(GetGroupsUrl, [State#state.access_token])),
	?INFO("Get user groups", []),
	case erl_weixin_request:get(Url, [], fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			Groups = rfc4627:get_field(RepJson, "groups", undefined),
			GtoupsInfo = lists:map(fun(GroupInfoJson) ->
				#weixin_group_info{
					id = rfc4627:get_field(GroupInfoJson, "id", undefined),
					name = binary_to_list(rfc4627:get_field(GroupInfoJson, "name", <<>>)),
					count = rfc4627:get_field(GroupInfoJson, "count", <<"0">>)
				}
			end, Groups),
			{reply, {ok, GtoupsInfo}, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({get_user_group, OpenId}, _From, State) ->
	GetUserGroupUrl = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=~s",
	Url = lists:flatten(io_lib:format(GetUserGroupUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"openid">>, list_to_binary(OpenId)}
	]}),
	?INFO("Get user group ~s", [OpenId]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			GroupId = rfc4627:get_field(RepJson, "groupid", undefined),
			{reply, {ok, GroupId}, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({rename_group, GroupId, Name}, _From, State) ->
	RenameGroupUrl = "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=~s",
	Url = lists:flatten(io_lib:format(RenameGroupUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"group">>, {obj, [
			{<<"id">>, GroupId},
			{<<"name">>, list_to_binary(Name)}
		]}}
	]}),
	?INFO("Rename group ~s as ~s", [GroupId, Name]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, _} ->
			{reply, ok, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({set_user_group, OpenId, GroupId}, _From, State) ->
	SetUserGroupUrl = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=~s",
	Url = lists:flatten(io_lib:format(SetUserGroupUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"openid">>, list_to_binary(OpenId)},
		{<<"to_groupid">>, GroupId}
	]}),
	?INFO("Set user ~p in group ~s", [OpenId, GroupId]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, _} ->
			{reply, ok, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({set_users_group, OpenIdList, GroupId}, _From, State) ->
	SetUsersGroupUrl = "https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=~s",
	Url = lists:flatten(io_lib:format(SetUsersGroupUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"openid_list">>, lists:map(fun list_to_binary/1, OpenIdList)},
		{<<"to_groupid">>, GroupId}
	]}),
	?INFO("Set users ~p in group ~s", [OpenIdList, GroupId]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, _} ->
			{reply, ok, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({set_user_remark, OpenId, Remark}, _From, State) ->
	SetUserRemarkUrl = "https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=~s",
	Url = lists:flatten(io_lib:format(SetUserRemarkUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"openid">>, list_to_binary(OpenId)},
		{<<"remark">>, list_to_binary(Remark)}
	]}),
	?INFO("Set user ~p remark as ~s", [OpenId, Remark]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, _} ->
			{reply, ok, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({get_user_info, OpenId, Lang}, _From, State) ->
	GetUserInfoUrl = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=~s&openid=~s&lang=~s",
	Url = lists:flatten(io_lib:format(GetUserInfoUrl, [State#state.access_token, OpenId, Lang])),
	?INFO("Get user ~p info", [OpenId]),
	case erl_weixin_request:get(Url, [], fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			Subscribe = erl_weixin_util:integer_to_boolean(rfc4627:get_field(RepJson, "subscribe", 0)),
			OpenId = binary_to_list(rfc4627:get_field(RepJson, "openid", undefined)),
			NickName = binary_to_list(rfc4627:get_field(RepJson, "nickname", undefined)),
			Sex = erl_weixin_util:sex(rfc4627:get_field(RepJson, "sex", 0)),
			City = binary_to_list(rfc4627:get_field(RepJson, "city", undefined)),
			Country = binary_to_list(rfc4627:get_field(RepJson, "country", undefined)),
			Province = binary_to_list(rfc4627:get_field(RepJson, "province", undefined)),
			Language = erl_weixin_util:language(binary_to_list(rfc4627:get_field(RepJson, "language", undefined))),
			HeadImgUrl = binary_to_list(rfc4627:get_field(RepJson, "headimgurl", undefined)),
			SubscribeTime = rfc4627:get_field(RepJson, "subscribe_time", undefined),
			UnionId = binary_to_list(rfc4627:get_field(RepJson, "unionid", <<>>)),
			UserInfo = #weixin_user_info{
				subscribe = Subscribe,
				open_id = OpenId,
				nickname = NickName,
				sex = Sex,
				city = City,
				country = Country,
				province = Province,
				language = Language,
				headimg_url = HeadImgUrl,
				subscribe_time = erl_weixin_util:ctime_to_datetime(SubscribeTime + erl_weixin_util:time_zone()),
				unionid = UnionId
			},
			{reply, {ok, UserInfo}, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({get_user_list, NextOpenId}, _From, State) ->
	Url =
		case NextOpenId of
			undefined ->
				?INFO("Get user list", []),
				io_lib:format("https://api.weixin.qq.com/cgi-bin/user/get?access_token=~s", [State#state.access_token]);
			_ ->
				?INFO("Get user list from ~s", [NextOpenId]),
				io_lib:format("https://api.weixin.qq.com/cgi-bin/user/get?access_token=~s&next_openid=~s",
					[State#state.access_token, NextOpenId])
		end,
	case erl_weixin_request:get(Url, [], fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			Total = rfc4627:get_field(RepJson, "total", undefined),
			Count = rfc4627:get_field(RepJson, "count", undefined),
			NextOpenId2 = binary_to_list(rfc4627:get_field(RepJson, "next_openid", undefined)),
			Data = rfc4627:get_field(RepJson, "data", undefined),
			OpenIdList = lists:map(fun binary_to_list/1, rfc4627:get_field(Data, "openid", undefined)),
			case NextOpenId2 =:= [] of
				true ->
					{reply, {ok, Total, Count, OpenIdList}, State};
				false ->
					{reply, {ok, Total, Count, OpenIdList, NextOpenId2}, State}
			end;
		Error ->
			{reply, Error, State}
	end;

handle_call({create_menu, Buttons}, _From, State) ->
	CreateMenuUrl = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=~s",
	Url = lists:flatten(io_lib:format(CreateMenuUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"button">>, erl_weixin_menu:to_json(Buttons)}
	]}),
	?INFO("Create menu", []),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, _} ->
			{reply, ok, State};
		Error ->
			{reply, Error, State}
	end;

handle_call(get_menu, _From, State) ->
	GetMenuUrl = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=~s",
	Url = lists:flatten(io_lib:format(GetMenuUrl, [State#state.access_token])),
	?INFO("Get menu", []),
	case erl_weixin_request:get(Url, [], fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			Button = rfc4627:get_field(RepJson, "menu", undefined),
			{reply, {ok, erl_weixin_menu:from_json(Button)}, State};
		Error ->
			{reply, Error, State}
	end;

handle_call(delete_menu, _From, State) ->
	DeleteMenuUrl = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=~s",
	Url = lists:flatten(io_lib:format(DeleteMenuUrl, [State#state.access_token])),
	?INFO("Delete menu", []),
	case erl_weixin_request:get(Url, [], fun erl_weixin_request:json_rep/3) of
		{ok, _, _} ->
			{reply, ok, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({create_qrcode, ExpireSeconds, qr_scene, SceneId}, _From, State) ->
	CreateQRCodeUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=~s",
	Url = lists:flatten(io_lib:format(CreateQRCodeUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"expire_seconds">>, ExpireSeconds},
		{<<"action_name">>, <<"QR_SCENE">>},
		{<<"action_info">>, {obj, [
			{<<"scene">>, {obj, [
				{<<"scene_id">>, SceneId}
			]}}
		]}}
	]}),
	?INFO("Create Qrcode qr_scene ~p", [SceneId]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			{reply, {ok, parse_qrcode_result(RepJson)}, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({create_qrcode, ExpireSeconds, qr_limit_scene, SceneId}, _From, State) when SceneId >= 1 andalso SceneId =< 100000 ->
	CreateQRCodeUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=~s",
	Url = lists:flatten(io_lib:format(CreateQRCodeUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"expire_seconds">>, ExpireSeconds},
		{<<"action_name">>, <<"QR_LIMIT_SCENE">>},
		{<<"action_info">>, {obj, [
			{<<"scene">>, {obj, [
				{<<"scene_id">>, SceneId}
			]}}
		]}}
	]}),
	?INFO("Create Qrcode qr_limit_scene ~p", [SceneId]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			{reply, {ok, parse_qrcode_result(RepJson)}, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({create_qrcode, ExpireSeconds, qr_limit_str_scene, SceneStr}, _From, State) ->
	CreateQRCodeUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=~s",
	Url = lists:flatten(io_lib:format(CreateQRCodeUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"expire_seconds">>, ExpireSeconds},
		{<<"action_name">>, <<"QR_LIMIT_STR_SCENE">>},
		{<<"action_info">>, {obj, [
			{<<"scene">>, {obj, [
				{<<"scene_str">>, SceneStr}
			]}}
		]}}
	]}),
	?INFO("Create Qrcode qr_limit_scene_str ~p", [SceneStr]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			{reply, {ok, parse_qrcode_result(RepJson)}, State};
		Error ->
			{reply, Error, State}
	end;

handle_call({create_short_url, Url}, _From, State) ->
	GetShortUrlUrl = "https://api.weixin.qq.com/cgi-bin/shorturl?access_token=~s",
	Url = lists:flatten(io_lib:format(GetShortUrlUrl, [State#state.access_token])),
	PostData = rfc4627:encode({obj, [
		{<<"action">>, <<"long2short">>},
		{<<"long_url">>, list_to_binary(Url)}
	]}),
	?INFO("Create short url : ~p", [Url]),
	case erl_weixin_request:post(Url, "text/json", [], PostData, fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			ShortUrl = binary_to_list(rfc4627:get_field(RepJson, "short_url", undefined)),
			{reply, {ok, ShortUrl}, State};
		Error ->
			{reply, Error, State}
	end.

handle_cast(_Request, State) ->
	{noreply, State}.

handle_info(token_expired, State) ->
	?INFO("Accesstoken is expired, refetch", []),
	case fetch_access_token(State#state.app_id, State#state.secret) of
		{ok, {AccessToken, ExpireTime}} ->
			set_expire_timer(ExpireTime),
			{noreply, State#state{access_token = AccessToken}};
		{error, Reason} ->
			{stop, Reason, State}
	end.

terminate(_Reason, _State) ->
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

access_token_url() ->
	"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=~s&secret=~s".

fetch_access_token(AppId, Secret) ->
	Url = lists:flatten(io_lib:format(access_token_url(), [AppId, Secret])),
	?INFO("Fetch accesstoken from ~p ...", [Url]),
	case erl_weixin_request:get(Url, [], fun erl_weixin_request:json_rep/3) of
		{ok, _, RepJson} ->
			AccessToken = rfc4627:get_field(RepJson, "access_token", undefined),
			ExpiresIn = rfc4627:get_field(RepJson, "expires_in", undefined),
			Now = erl_weixin_util:datetime_to_ctime(erlang:localtime()) + ExpiresIn,
			ExpireTime = erl_weixin_util:ctime_to_datetime(Now),
			?INFO("fetch accesstoken finished: ~s", [AccessToken]),
			mnesia:transaction(fun() ->
				mnesia:write(#weixin_token{
					app_id = AppId,
					access_token = AccessToken,
					expire_time = ExpireTime})
			end),
			{ok, {AccessToken, ExpireTime}};
		Error ->
			Error
	end.

is_expired(Time) ->
	Now = erl_weixin_util:datetime_to_ctime(erlang:localtime()),
	CTime = erl_weixin_util:datetime_to_ctime(Time),
	CTime =< Now.

set_expire_timer(ExpireTime) ->
	Now = erl_weixin_util:datetime_to_ctime(erlang:localtime()),
	Expire = erl_weixin_util:datetime_to_ctime(ExpireTime),
	Seconds = Expire - Now,
	?INFO("Accesstoken expires in ~s hours", [float_to_list(Seconds / (60 * 60), [{decimals, 3}])]),
	timer:send_after(timer:seconds(Seconds), token_expired).

load_access_token(AppId, Secret) ->
	Q = qlc:q([Item || #weixin_token{app_id = QAppId} = Item <-
		mnesia:table(weixin_token), AppId =:= QAppId]),
	{atomic, Result} = mnesia:transaction(fun() -> qlc:e(Q) end),
	Refetch = case Result of
				  [] ->
					  ?INFO("Load accesstoken from mnesia failed: not_found", []),
					  true;
				  [Item] ->
					  case is_expired(Item#weixin_token.expire_time) of
						  true ->
							  ?INFO("Load accesstoken from mnesia finished, but it's expired", []),
							  true;
						  false ->
							  ?INFO("Load accesstoken from mnesia finished", []),
							  false
					  end
			  end,
	case Refetch of
		true ->
			case fetch_access_token(AppId, Secret) of
				{ok, {AccessToken, ExpireTime}} ->
					{ok, {AccessToken, ExpireTime}};
				Error ->
					Error
			end;
		false ->
			[QItem] = Result,
			{ok, {QItem#weixin_token.access_token, QItem#weixin_token.expire_time}}
	end.

parse_qrcode_result(Json) ->
	Ticket = rfc4627:get_field(Json, "ticket", undefined),
	ExpireSeconds = rfc4627:get_field(Json, "expire_seconds", undefined),
	Url = rfc4627:get_field(Json, "url", undefined),
	#weixin_qrcode{
		ticket = Ticket,
		expire_seconds = ExpireSeconds,
		url = Url
	}.

-type media_type() :: image | voice | video | thumb.

-spec upload_media(Server :: pid(), Filename :: string(), FileData :: binary(), Type :: media_type()) ->
	{ok, MediaId :: string(), CreateTime :: calendar:datetime()} |
	{error, Reason :: term()}.
upload_media(Server, Filename, FileData, Type) ->
	gen_server:call(Server, {upload_media, Filename, FileData, Type}).

-spec download_media(Server :: pid(), MediaId :: string()) ->
	{ok, FileData :: binary(), MimeType :: string(), FileName :: string()} |
	{error, Reason :: term()}.
download_media(Server, MediaId) ->
	gen_server:call(Server, {download_media, MediaId}).

-spec create_group(Server :: pid(), Name :: string()) ->
	{ok, GroupId :: integer()} |
	{error, Reason :: term()}.
create_group(Server, Name) ->
	gen_server:call(Server, {create_group, Name}).

-spec get_groups(Server :: pid()) ->
	{ok, [GroupsInfo :: #weixin_group_info{}]} |
	{error, Reason :: term()}.
get_groups(Server) ->
	gen_server:call(Server, get_groups).

-spec get_user_group(Server :: pid(), OpenId :: string()) ->
	{ok, GroupId :: integer()} |
	{error, Reason :: term()}.
get_user_group(Server, OpenId) ->
	gen_server:call(Server, {get_user_group, OpenId}).

-spec rename_group(Server :: pid(), GroupId :: integer(), Name :: string()) ->
	ok |
	{error, Reason :: term()}.
rename_group(Server, GroupId, Name) ->
	gen_server:call(Server, {rename_group, GroupId, Name}).

-spec set_user_group(Server :: pid(), OpenId :: string(), GroupId :: integer()) ->
	ok |
	{error, Reason :: term()}.
set_user_group(Server, OpenId, GroupId) ->
	gen_server:call(Server, {set_user_group, OpenId, GroupId}).

-spec set_users_group(Server :: pid(), OpenIdList :: [string()], GroupId :: integer()) ->
	ok |
	{error, Reason :: term()}.
set_users_group(Server, OpenIdList, GroupId) ->
	gen_server:call(Server, {set_users_group, OpenIdList, GroupId}).

-spec set_user_remark(Server :: pid(), OpenId :: string(), Remark :: string()) ->
	ok |
	{error, Reason :: term()}.
set_user_remark(Server, OpenId, Remark) ->
	gen_server:call(Server, {set_user_remark, OpenId, Remark}).

-spec get_user_info(Server :: pid(), OpenId :: string(), Lang :: weixin_lang()) ->
	{ok, #weixin_user_info{}} |
	{error, Reason :: term()}.
get_user_info(Server, OpenId, Lang) ->
	gen_server:call(Server, {get_user_info, OpenId, Lang}).

-spec get_user_info(Server :: pid(), OpenId :: string()) ->
	{ok, #weixin_user_info{}} |
	{error, Reason :: term()}.
get_user_info(Server, OpenId) ->
	gen_server:call(Server, {get_user_info, OpenId, zh_CN}).

-spec get_user_list(Server :: pid()) ->
	{ok, Total :: integer(), Count :: integer(), [OpenId :: string()]} |
	{ok, Total :: integer(), Count :: integer(), [OpenId :: string()], NextOpenId :: string()} |
	{error, Reason :: term()}.
get_user_list(Server) ->
	gen_server:call(Server, {get_user_list, undefined}).

-spec get_user_list(Server :: pid(), NextOpenId :: string()) ->
	{ok, Total :: integer(), Count :: integer(), [OpenId :: string()]} |
	{ok, Total :: integer(), Count :: integer(), [OpenId :: string()], NextOpenId :: string()} |
	{error, Reason :: term()}.
get_user_list(Server, NextOpenId) ->
	gen_server:call(Server, {get_user_list, NextOpenId}).

-spec create_menu(Server :: pid(), Buttons :: [#weixin_button{}]) ->
	ok |
	{error, Reason :: term()}.
create_menu(Server, Buttons) ->
	gen_server:call(Server, {create_menu, Buttons}).

-spec get_menu(Server :: pid()) ->
	{ok, Buttons :: [#weixin_button{}]} |
	{error, Reason :: term()}.
get_menu(Server) ->
	gen_server:call(Server, get_menu).

-spec delete_menu(Server :: pid()) ->
	ok |
	{error, Reason :: term()}.
delete_menu(Server) ->
	gen_server:call(Server, delete_menu).

-spec create_qrcode(Server :: pid(), ExpireSeconds :: integer(), QrCodeType :: weixin_qrcode_type(), Scene :: integer() | string()) ->
	{ok, #weixin_qrcode{}} |
	{error, Reason :: term()}.
create_qrcode(Server, ExpireSeconds, qr_scene, Scene) ->
	gen_server:call(Server, {create_qrcode, ExpireSeconds, qr_scene, Scene});

create_qrcode(Server, ExpireSeconds, qr_limit_scene, Scene) when Scene >= 1 andalso Scene =< 100000 ->
	gen_server:call(Server, {create_qrcode, ExpireSeconds, qr_limit_scene, Scene});

create_qrcode(Server, ExpireSeconds, qr_limit_str_scene, Scene) ->
	gen_server:call(Server, {create_qrcode, ExpireSeconds, qr_limit_str_scene, Scene}).

-spec get_qrcode_image(Ticket :: string()) ->
	{ok, ImageBinary :: binary()} |
	{error, Reason :: term()}.
get_qrcode_image(Ticket) ->
	Url = lists:flatten(io_lib:format("https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=~s", [Ticket])),
	case erl_weixin_request:get(Url, [], fun erl_weixin_request:raw_rep/3) of
		{ok, _, RepBinary} ->
			{ok, RepBinary};
		Error ->
			Error
	end.

-spec create_short_url(Server :: pid(), Url :: string()) ->
	{ok, ShortUrl :: string()}|
	{error, Reason :: term()}.
create_short_url(Server, Url) ->
	gen_server:call(Server, {create_short_url, Url}).