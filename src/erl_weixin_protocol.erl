%% -*- coding: utf-8 -*-

-module(erl_weixin_protocol).
-export([init/3, handle/2, terminate/3]).
-include("erl_weixin.hrl").
-include("erl_weixin_logger.hrl").
-include_lib("xmerl/include/xmerl.hrl").

-record(state, {
	mod :: module(),
	token :: string(),
	app_id :: string(),
	state :: any(),
	aes_key :: string()
}).

-record(url_param, {
	timestamp,
	signature,
	nonce,
	encrypt_type,
	msg_signature
}).

-spec init(Type :: term(), Req :: term(), Options :: weixin_options()) -> {ok, NewReq :: term(), State :: term()} | {error, Reason :: any()}.
init(_Type, Req, Options) ->
	Mod = proplists:get_value(handler, Options),
	case Mod:weixin_init() of
		{ok, State} ->
			{ok, Req, #state{
				mod = Mod,
				token = proplists:get_value(token, Options),
				app_id = proplists:get_value(app_id, Options),
				aes_key = proplists:get_value(aes_key, Options),
				state = State
			}};
		{error, _} = Error ->
			Error
	end.

handle(Req, State) ->
	{Method, Req2} = cowboy_req:method(Req),
	handle(Method, Req2, State).

terminate(Reason, _Req, #state{mod = Mod, state = State}) ->
	Mod:weixin_terminate(Reason, State).

handle(<<"GET">>, Req, State) ->
	{#url_param{timestamp = TimeStamp, nonce = NOnce, signature = Signature}, Req2} = parse_url_param(Req),
	case erl_weixin_signature:check([State#state.token, TimeStamp, NOnce], Signature) of
		true ->
			?INFO("Verify weixin service finished", []),
			{EchoStr, Req3} = cowboy_req:qs_val(<<"echostr">>, Req2),
			{ok, Req4} = cowboy_req:reply(200, [
				{<<"content-type">>, <<"text/plain">>}
			], EchoStr, Req3),
			{ok, Req4, State};
		false ->
			?INFO("Verify weixin service failed", []),
			{ok, Req2, State}
	end;

handle(<<"POST">>, Req, State) ->
	{#url_param{timestamp = TimeStamp, nonce = NOnce, signature = Signature} = UrlParam, Req2} = parse_url_param(Req),
	case erl_weixin_signature:check([State#state.token, TimeStamp, NOnce], Signature) of
		true ->
			{ok, RawBodyBinary, Req3} = cowboy_req:body(Req2),
			case decode_body(UrlParam, RawBodyBinary, State) of
				{ok, BodyBinary} ->
					{XmlDoc, _} = xmerl_scan:string(binary_to_list(BodyBinary)),
					MsgHeader = erl_weixin_parse_request:parse_msg_header(XmlDoc),
					case MsgHeader#weixin_message_header.msg_type of
						"event" ->
							MsgBody = erl_weixin_parse_request:parse_event_body(XmlDoc),
							handle_event(UrlParam, MsgHeader, MsgBody, Req3, State);
						_ ->
							MsgBody = erl_weixin_parse_request:parse_msg_body(MsgHeader, XmlDoc),
							handle_request(UrlParam, MsgHeader, MsgBody, Req3, State)
					end;
				{error, _} ->
					{ok, Req3, State}
			end;
		false ->
			{ok, Req2, State}
	end.

handle_request(_UrlParam, _RequestMsgHeader, {error, unkown_msg_type}, Req, State) ->
	{ok, Req, State};

handle_request(UrlParam, RequestMsgHeader, MsgBody, Req, #state{mod = Mod} = State) ->
	?INFO("Handle request: ~p: ~p", [RequestMsgHeader, MsgBody]),
	Reply = Mod:weixin_message(RequestMsgHeader, MsgBody, State#state.state),
	reply_msg(UrlParam, Req, RequestMsgHeader, Reply, State).

handle_event(_UrlParam, _RequestMsgHeader, {error, unkown_msg_type}, Req, State) ->
	{ok, Req, State};

handle_event(UrlParam, RequestMsgHeader, MsgBody, Req, #state{mod = Mod} = State) ->
	?INFO("Handle event: ~p: ~p", [RequestMsgHeader, MsgBody]),
	Reply = Mod:weixin_event(RequestMsgHeader, MsgBody, State#state.state),
	reply_msg(UrlParam, Req, RequestMsgHeader, Reply, State).

reply_msg(_UrlParam, Req, _RequestMsgHeader, {noreply, NewHandlerState}, State) ->
	{ok, Req, State#state{state = NewHandlerState}};

reply_msg(UrlParam, Req, RequestMsgHeader, {reply, ReplyMsgBody, NewHandlerState}, State) ->
	?INFO("Reply: ~p", [ReplyMsgBody]),
	case erl_weixin_build_response:build(RequestMsgHeader, ReplyMsgBody) of
		{error, unkown_msg_type} ->
			{ok, Req, State#state{state = NewHandlerState}};
		{ok, ReplyXmlDocStr} ->
			case encode_body(UrlParam, iolist_to_binary(ReplyXmlDocStr), State) of
				{ok, EncodedXmlDoc} ->
					{ok, Req2} = cowboy_req:reply(200, [
						{<<"content-type">>, <<"text/xml">>}
					], EncodedXmlDoc, Req),
					{ok, Req2, State#state{state = NewHandlerState}};
				_ ->
					{ok, Req, State#state{state = NewHandlerState}}
			end
	end.

parse_url_param(Req) ->
	{TimeStamp, Req2} = cowboy_req:qs_val(<<"timestamp">>, Req),
	{NOnce, Req3} = cowboy_req:qs_val(<<"nonce">>, Req2),
	{Signature, Req4} = cowboy_req:qs_val(<<"signature">>, Req3),
	{EncryptType, Req5} = cowboy_req:qs_val(<<"encrypt_type">>, Req4),
	{MsgSignature, Req6} = cowboy_req:qs_val(<<"msg_signature">>, Req5),
	ToList = fun(Binary) ->
		case is_binary(Binary) of
			true ->
				binary_to_list(Binary);
			false ->
				undefined
		end
	end,
	{#url_param{
		timestamp = ToList(TimeStamp),
		nonce = ToList(NOnce),
		signature = ToList(Signature),
		encrypt_type = ToList(EncryptType),
		msg_signature = ToList(MsgSignature)
	}, Req6}.

decode_body(#url_param{encrypt_type = "aes"} = UrlParam, Data, State) ->
	{XmlDoc, _} = xmerl_scan:string(binary_to_list(Data)),
	[#xmlText{value = EncryptMsg}] = xmerl_xpath:string("Encrypt/text()", XmlDoc),
	case erl_weixin_signature:check([State#state.token, UrlParam#url_param.timestamp,
		UrlParam#url_param.nonce, EncryptMsg], UrlParam#url_param.msg_signature) of
		true ->
			erl_weixin_encrypt:decrypt_aes(State#state.aes_key, State#state.app_id, base64:decode(EncryptMsg));
		false ->
			{error, bad_signature}
	end;

decode_body(_, Data, _) ->
	{ok, Data}.

encode_body(#url_param{encrypt_type = "aes", nonce = NOnce, timestamp = TimeStamp}, XmlDocBin, State) ->
	{ok, EData} = erl_weixin_encrypt:encrypt_aes(State#state.aes_key, State#state.app_id, XmlDocBin),
	RData = binary_to_list(base64:encode(EData)),
	MsgSignature = erl_weixin_signature:calc([
		State#state.token,
		NOnce,
		TimeStamp,
		RData
	]),
	XmlDoc = [
		{'MsgSignature', [MsgSignature]},
		{'TimeStamp', [TimeStamp]},
		{'Nonce', [NOnce]},
		{'Encrypt', [RData]}
	],
	{ok, xmerl:export_simple_element({xml, XmlDoc}, xmerl_xml)};

encode_body(_, Data, _) ->
	{ok, Data}.
