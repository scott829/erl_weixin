%% -*- coding: utf-8 -*-

-module(erl_weixin_encrypt).
-export([decrypt_aes/3, encrypt_aes/3]).
-define(RandEncryptStrLen, 16).
-define(AesKeySize, 32).

decrypt_aes(AesKey, AppId, Data) ->
	AesKeyBinary = base64:decode(AesKey ++ "="),
	<<Iv:16/binary, _/binary>> = AesKeyBinary,
	DecryptData = crypto:block_decrypt(aes_cbc128, AesKeyBinary, Iv, Data),
	ResultSize = byte_size(Data) - binary:last(DecryptData),
	{ResultData, _} = split_binary(DecryptData, ResultSize),
	<<_:?RandEncryptStrLen/binary, MsgLen:32/unsigned-integer, TailBinary/binary>> = ResultData,
	<<Msg:MsgLen/binary, RAppId/binary>> = TailBinary,
	case binary_to_list(RAppId) =:= AppId of
		true ->
			{ok, Msg};
		false ->
			{error, bad_app_id}
	end.

encrypt_aes(AesKey, AppId, Data) ->
	AesKeyBinary = base64:decode(AesKey ++ "="),
	DataSize = byte_size(Data),
	EncryptData = iolist_to_binary([crypto:rand_bytes(?RandEncryptStrLen), <<DataSize:32/unsigned-integer>>, Data, AppId]),
	Padding = ?AesKeySize - byte_size(EncryptData) rem ?AesKeySize,
	EncryptData2 = iolist_to_binary([EncryptData, lists:duplicate(Padding, Padding)]),
	<<Iv:16/binary, _/binary>> = AesKeyBinary,
	EncryptedData = crypto:block_encrypt(aes_cbc128, AesKeyBinary, Iv, EncryptData2),
	{ok, EncryptedData}.
