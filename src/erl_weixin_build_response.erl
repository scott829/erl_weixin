%% -*- coding: utf-8 -*-

-module(erl_weixin_build_response).
-export([build/2]).
-include("erl_weixin.hrl").

add_field(Name, Value, FieldList) ->
	case Value of
		undefined -> FieldList;
		Value -> [{Name, [Value]} | FieldList]
	end.

add_children(Name, Children, FieldList) ->
	[{Name, Children} | FieldList].

header(RequestHeader, Type) ->
	[
		{'ToUserName', [RequestHeader#weixin_message_header.from_user]},
		{'FromUserName', [RequestHeader#weixin_message_header.to_user]},
		{'MsgType', [Type]},
		{'CreateTime', [erlang:integer_to_list(erl_weixin_util:datetime_to_ctime(erlang:universaltime()))]}
	].

export_xml(XmlDoc) ->
	xmerl:export_simple_element({xml, XmlDoc}, xmerl_xml).

build(RequestHeader, #weixin_text_message{content = Content}) ->
	X1 = header(RequestHeader, "text"),
	X2 = add_field('Content', Content, X1),
	{ok, export_xml(X2)};

build(RequestHeader, #weixin_image_message{media_id = MediaId}) ->
	ImgXml = add_field('MediaId', MediaId, []),
	X1 = header(RequestHeader, "image"),
	X2 = add_children('Image', ImgXml, X1),
	{ok, export_xml(X2)};

build(RequestHeader, #weixin_voice_message{media_id = MediaId}) ->
	VoiceXml = add_field('MediaId', MediaId, []),
	X1 = header(RequestHeader, "voice"),
	X2 = add_children('Voice', VoiceXml, X1),
	{ok, export_xml(X2)};

build(RequestHeader, #weixin_video_message{media_id = MediaId, title = Title, description = Description}) ->
	VideoXml1 = add_field('MediaId', MediaId, []),
	VideoXml2 = add_field('Title', Title, VideoXml1),
	VideoXml3 = add_field('Description', Description, VideoXml2),
	X1 = header(RequestHeader, "video"),
	X2 = add_children('Voice', VideoXml3, X1),
	{ok, export_xml(X2)};

build(RequestHeader, #weixin_music_message{title = Title, description = Description,
	url = MusicURL, hq_url = HQMusicUrl, thumb_media_id = ThumbMediaId}) ->
	MusicXml1 = add_field('Title', Title, []),
	MusicXml2 = add_field('Description', Description, MusicXml1),
	MusicXml3 = add_field('MusicURL', MusicURL, MusicXml2),
	MusicXml4 = add_field('HQMusicUrl', HQMusicUrl, MusicXml3),
	MusicXml5 = add_field('ThumbMediaId', ThumbMediaId, MusicXml4),
	X1 = header(RequestHeader, "music"),
	X2 = add_children('Voice', MusicXml5, X1),
	{ok, export_xml(X2)};

build(RequestHeader, #weixin_news_message{items = Items}) ->
	ItemsXml = lists:map(fun(#weixin_news_item{title = Title, description = Description, pic_url = PicUrl, url = Url}) ->
		IX1 = add_field('Title', Title, []),
		IX2 = add_field('Description', Description, IX1),
		IX3 = add_field('PicUrl', PicUrl, IX2),
		IX4 = add_field('Url', Url, IX3),
		{'Item', IX4}
	end, Items),
	X1 = header(RequestHeader, "news"),
	X2 = add_field('ArticleCount', length(Items), X1),
	X3 = add_children('Articles', ItemsXml, X2),
	{ok, export_xml(X3)};

build(_RequestMsgHeader, _MsgBody) ->
	{error, unkown_msg_type}.