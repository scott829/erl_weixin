%% -*- coding: utf-8 -*-

-module(erl_weixin_parse_request).
-export([parse_msg_header/1, parse_msg_body/2, parse_event_body/1]).
-include_lib("xmerl/include/xmerl.hrl").
-include("erl_weixin.hrl").

ctime(Value) ->
	erl_weixin_util:ctime_to_datetime(list_to_integer(Value) + erl_weixin_util:time_zone()).

utf8(Value) ->
	xmerl_ucs:to_utf8(Value).

get_value(XmlDoc, Path) ->
	case xmerl_xpath:string(Path, XmlDoc) of
		[#xmlText{value = Value}] -> Value;
		[] -> undefined
	end.

get_value(XmlDoc, Path, ConvFun) ->
	case get_value(XmlDoc, Path) of
		undefined -> undefined;
		Value -> ConvFun(Value)
	end.

parse_msg_header(XmlDoc) ->
	#weixin_message_header{
		to_user = get_value(XmlDoc, "ToUserName/text()", fun utf8/1),
		from_user = get_value(XmlDoc, "FromUserName/text()", fun utf8/1),
		create_time = get_value(XmlDoc, "CreateTime/text()", fun ctime/1),
		msg_type = get_value(XmlDoc, "MsgType/text()", fun utf8/1),
		msg_id = get_value(XmlDoc, "MsgId/text()")
	}.

parse_msg_body(#weixin_message_header{msg_type = "text"}, XmlDoc) ->
	#weixin_text_message{
		content = get_value(XmlDoc, "Content/text()", fun utf8/1)
	};

parse_msg_body(#weixin_message_header{msg_type = "image"}, XmlDoc) ->
	#weixin_image_message{
		pic_url = get_value(XmlDoc, "PicUrl/text()", fun utf8/1),
		media_id = get_value(XmlDoc, "MediaId/text()", fun utf8/1)
	};

parse_msg_body(#weixin_message_header{msg_type = "voice"}, XmlDoc) ->
	#weixin_voice_message{
		format = get_value(XmlDoc, "Format/text()", fun utf8/1),
		media_id = get_value(XmlDoc, "MediaId/text()", fun utf8/1),
		recognition = get_value(XmlDoc, "Recognition/text()", fun utf8/1)
	};

parse_msg_body(#weixin_message_header{msg_type = "video"}, XmlDoc) ->
	#weixin_video_message{
		thumb_media_id = get_value(XmlDoc, "ThumbMediaId/text()", fun utf8/1),
		media_id = get_value(XmlDoc, "MediaId/text()", fun utf8/1)
	};

parse_msg_body(#weixin_message_header{msg_type = "location"}, XmlDoc) ->
	#weixin_location_message{
		location_x = get_value(XmlDoc, "Location_X/text()", fun list_to_float/1),
		location_y = get_value(XmlDoc, "Location_Y/text()", fun list_to_float/1),
		scale = get_value(XmlDoc, "Scale/text()", fun list_to_integer/1),
		label = get_value(XmlDoc, "Label/text(), fun utf8/1")
	};

parse_msg_body(#weixin_message_header{msg_type = "link"}, XmlDoc) ->
	#weixin_link_message{
		title = get_value(XmlDoc, "Title/text()", fun utf8/1),
		description = get_value(XmlDoc, "Description/text()", fun utf8/1),
		url = get_value(XmlDoc, "Url/text()", fun utf8/1)
	};

parse_msg_body(_Header, _XmlDoc) ->
	{error, unkown_msg_type}.

parse_event_body(XmlDoc) ->
	EventType = get_value(XmlDoc, "Event/text()", fun utf8/1),
	parse_event_body(EventType, XmlDoc).

parse_event_body("subscribe", XmlDoc) ->
	#weixin_event_subscribe{
		event_key = get_value(XmlDoc, "EventKey/text()", fun utf8/1),
		ticket = get_value(XmlDoc, "Ticket/text()", fun utf8/1)
	};

parse_event_body("unsubscribe", _XmlDoc) ->
	#weixin_event_unsubscribe{};

parse_event_body("SCAN", XmlDoc) ->
	#weixin_event_scan{
		event_key = get_value(XmlDoc, "EventKey/text()", fun utf8/1),
		ticket = get_value(XmlDoc, "Ticket/text()", fun utf8/1)
	};

parse_event_body("LOCATION", XmlDoc) ->
	#weixin_event_location{
		latitude = get_value(XmlDoc, "Latitude/text()", fun list_to_float/1),
		longitude = get_value(XmlDoc, "Longitude/text()", fun list_to_float/1),
		precision = get_value(XmlDoc, "Precision/text()", fun list_to_float/1)
	};

parse_event_body("CLICK", XmlDoc) ->
	#weixin_event_click{
		event_key = get_value(XmlDoc, "EventKey/text()", fun utf8/1)
	};

parse_event_body("VIEW", XmlDoc) ->
	#weixin_event_view{
		event_key = get_value(XmlDoc, "EventKey/text()", fun utf8/1)
	};

parse_event_body("scancode_push", XmlDoc) ->
	#weixin_event_scancode_push{
		event_key = get_value(XmlDoc, "EventKey/text()", fun utf8/1),
		scancode_info = get_value(XmlDoc, "EventKey/text()", fun utf8/1),
		scan_type = get_value(XmlDoc, "ScanType/text()", fun utf8/1),
		scan_result = get_value(XmlDoc, "ScanResult/text()", fun utf8/1)
	};

parse_event_body("scancode_waitmsg", XmlDoc) ->
	#weixin_event_scancode_wait{
		event_key = get_value(XmlDoc, "EventKey/text()", fun utf8/1),
		scancode_info = get_value(XmlDoc, "EventKey/text()", fun utf8/1),
		scan_type = get_value(XmlDoc, "ScanType/text()", fun utf8/1),
		scan_result = get_value(XmlDoc, "ScanResult/text()", fun utf8/1)
	};

parse_event_body("pic_sysphoto", XmlDoc) ->
	#weixin_event_pic{
		event_key = get_value(XmlDoc, "EventKey/text()", fun utf8/1),
		send_type = sysphoto,
		send_pics_info = parse_pics_info(xmerl_xpath:string("SendPicsInfo/PicList/item/PicMd5Sum/text()", XmlDoc))
	};

parse_event_body("pic_photo_or_album", XmlDoc) ->
	#weixin_event_pic{
		event_key = get_value(XmlDoc, "EventKey/text()", fun utf8/1),
		send_type = photo_or_album,
		send_pics_info = parse_pics_info(xmerl_xpath:string("SendPicsInfo/PicList/item/PicMd5Sum/text()", XmlDoc))
	};

parse_event_body("pic_weixin", XmlDoc) ->
	#weixin_event_pic{
		event_key = get_value(XmlDoc, "EventKey/text()", fun utf8/1),
		send_type = weixin,
		send_pics_info = parse_pics_info(xmerl_xpath:string("SendPicsInfo/PicList/item/PicMd5Sum/text()", XmlDoc))
	}.

parse_pics_info(XmlNodeList) ->
	lists:map(fun([#xmlText{value = Value}]) ->
		#weixin_pic_info{md5 = Value}
	end, XmlNodeList).
