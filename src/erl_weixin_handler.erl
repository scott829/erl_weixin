%% -*- coding: utf-8 -*-

-module(erl_weixin_handler).
-include("erl_weixin.hrl").

-type weixin_request() ::
#weixin_text_message{} |
#weixin_image_message{} |
#weixin_voice_message{} |
#weixin_video_message{} |
#weixin_location_message{} |
#weixin_link_message{}
.

-type weixin_response() ::
#weixin_text_message{} |
#weixin_image_message{} |
#weixin_voice_message{} |
#weixin_video_message{} |
#weixin_music_message{} |
#weixin_news_message{}
.

-type weixin_event() ::
#weixin_event_subscribe{} |
#weixin_event_unsubscribe{} |
#weixin_event_scan{} |
#weixin_event_location{} |
#weixin_event_click{} |
#weixin_event_view{}
.

-callback weixin_init() ->
	{ok, State :: any()} |
	{error, Reason :: term()}.

-callback weixin_message(MsgHeader :: #weixin_message_header{}, Msg :: weixin_request(), State :: any()) ->
	{noreply, State} |
	{reply, Msg :: weixin_response(), State}.

-callback weixin_event(MsgHeader :: #weixin_message_header{}, Event :: weixin_event(), State :: any()) ->
	{noreply, State} |
	{reply, Msg :: weixin_response(), State}.

-callback weixin_terminate(Reason :: term(), State :: any()) -> ok.
