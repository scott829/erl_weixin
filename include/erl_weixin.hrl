-ifndef(weixin).
-define(weixin, true).

-ifndef(UTF8).
-define(UTF8(Text), xmerl_ucs:to_utf8(Text)).
-endif.

-type weixin_option() ::
{handler, Handler :: module()} |
{token, Token :: string()} |
{app_id, AppId :: string()} |
{secret, Secret :: string()} |
{aes_key, AexKey :: string()}.

-type weixin_options() :: [weixin_option()].

%% 微信消息头
-record(weixin_message_header, {
	to_user :: string(),
	from_user :: string(),
	create_time :: calendar:datetime(),
	msg_type :: string(),
	msg_id :: string()
}).

-record(weixin_text_message, {
	content :: string()
}).

-record(weixin_image_message, {
	media_id :: string(),
	pic_url :: string()
}).

-record(weixin_voice_message, {
	media_id :: string(),
	format :: string(),
	recognition :: string()
}).

-record(weixin_video_message, {
	media_id :: string(),
	title :: string(),
	description :: string(),
	thumb_media_id :: string()
}).

-record(weixin_location_message, {
	location_x :: float(),
	location_y :: float(),
	scale :: float(),
	label :: string()
}).

-record(weixin_link_message, {
	url :: string(),
	title :: string(),
	description :: string()
}).

-record(weixin_music_message, {
	url :: string(),
	hq_url :: string(),
	title :: string(),
	description :: string(),
	thumb_media_id :: string()
}).

-record(weixin_news_item, {
	title :: string(),
	description :: string(),
	pic_url :: string(),
	url :: string()
}).

-record(weixin_news_message, {
	items :: [weixin_news_item]
}).

-record(weixin_event_subscribe, {
	event_key :: string(),
	ticket :: string()
}).

-record(weixin_event_unsubscribe, {
}).

-record(weixin_event_scan, {
	event_key :: string(),
	ticket :: string()
}).

-record(weixin_event_location, {
	latitude :: float(),
	longitude :: float(),
	precision :: float()
}).

-record(weixin_event_click, {
	event_key :: string()
}).

-record(weixin_event_view, {
	event_key :: string()
}).

-record(weixin_event_scancode_push, {
	event_key :: string(),
	scancode_info :: string(),
	scan_type :: string(),
	scan_result :: string()
}).

-record(weixin_event_scancode_wait, {
	event_key :: string(),
	scancode_info :: string(),
	scan_type :: string(),
	scan_result :: string()
}).

-type weixin_send_pic_type() :: sysphoto | photo_or_album | weixin.

-record(weixin_pic_info, {
	md5 :: string()
}).

-record(weixin_event_pic, {
	event_key :: string(),
	send_type :: weixin_send_pic_type(),
	send_pics_info :: [#weixin_pic_info{}]
}).

-record(weixin_event_location_select, {
	event_key :: string(),
	location_x :: float(),
	location_y :: float(),
	scale :: integer(),
	label :: string(),
	poi_name :: string()
}).

-record(weixin_download_media, {
	mime :: string(),
	filename :: string(),
	data :: binary()
}).

-record(weixin_group_info, {
	id :: integer(),
	name :: string(),
	count :: integer()
}).

-type weixin_lang() :: 'zh_CN' | 'zh_TW' | 'en'.

-type weixin_sex() :: gentleman | lady | unknown.

-record(weixin_user_info, {
	subscribe :: boolean(),
	open_id :: string(),
	nickname :: string(),
	sex :: weixin_sex(),
	language :: weixin_lang(),
	city :: string(),
	province :: string(),
	country :: string(),
	headimg_url :: string(),
	subscribe_time :: calendar:datetime(),
	unionid :: string()
}).

-type weixin_button_type() :: click | view | scancode_push | scancode_wait |
pic_sysphoto | pic_photo_or_album | pic_weixin | location_select.

-record(weixin_button, {
	name :: string(),
	type :: weixin_button_type(),
	key :: string(),
	url :: string(),
	sub_buttons :: [#weixin_button{}]
}).

-type weixin_qrcode_type() :: qr_scene | qr_limit_scene | qr_limit_str_scene.

-record(weixin_qrcode, {
	ticket :: string(),
	expire_seconds :: integer(),
	url :: string()
}).

-endif.